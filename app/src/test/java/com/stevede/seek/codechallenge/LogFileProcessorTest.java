package com.stevede.seek.codechallenge;

import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class LogFileProcessorTest {

    @Test
    public void testGetMinimumCarPeriodWhenMultiple() throws Exception {
        int recordCount = 2;
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
        LogRecord logRecord1 = new LogRecord(sdf.parse("2020-01-01T12:30:00"), 10, 30);
        LogRecord logRecord2 = new LogRecord(sdf.parse("2020-01-01T13:00:00"), 10, 30);
        List<LogRecord> logRecords = Arrays.asList(
                logRecord1,
                logRecord2,
                new LogRecord(sdf.parse("2020-01-01T13:30:00"), 11, 30)
        );
        LogFile logFile = new LogFile(logRecords);
        LogFileProcessor logFileProcessor = new LogFileProcessor(logFile);


        List<LogRecord> minimumCarPeriodLogRecords = logFileProcessor.getMinimumCarPeriod(recordCount, 30);
        assertEquals(recordCount, minimumCarPeriodLogRecords.size());
        assertEquals(logRecord1.getDate(), minimumCarPeriodLogRecords.get(0).getDate());
        assertEquals(logRecord2.getDate(), minimumCarPeriodLogRecords.get(1).getDate());
    }

    @Test
    public void testGetMinimumCarPeriodWhenSingleSelectedGetsFirstRecord() throws Exception {
        int recordCount = 1;
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
        LogRecord logRecord1 = new LogRecord(sdf.parse("2020-01-01T12:30:00"), 10, 30);
        LogRecord logRecord2 = new LogRecord(sdf.parse("2020-01-01T13:00:00"), 10, 30);
        List<LogRecord> logRecords = Arrays.asList(
                logRecord1,
                logRecord2,
                new LogRecord(sdf.parse("2020-01-01T13:30:00"), 11, 30)
        );
        LogFile logFile = new LogFile(logRecords);
        LogFileProcessor logFileProcessor = new LogFileProcessor(logFile);


        List<LogRecord> minimumCarPeriodLogRecords = logFileProcessor.getMinimumCarPeriod(recordCount, 30);
        assertEquals(logRecord1.getDate(), minimumCarPeriodLogRecords.get(0).getDate());
        assertEquals(recordCount, minimumCarPeriodLogRecords.size());
    }


    @Test
    public void testGetMinimumCarPeriodWhenMultipleWindowsAreMerged() throws Exception {
        int recordCount = 2;
        int intervalSize = 60;
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
        LogRecord logRecord1 = new LogRecord(sdf.parse("2020-01-01T12:30:00"), 10, 30);
        LogRecord logRecord2 = new LogRecord(sdf.parse("2020-01-01T13:00:00"), 7, 30);
        LogRecord logRecord3 = new LogRecord(sdf.parse("2020-01-01T13:30:00"), 8, 30);
        LogRecord logRecord4 = new LogRecord(sdf.parse("2020-01-01T14:00:00"), 9, 30);
        List<LogRecord> logRecords = Arrays.asList(
                logRecord1,
                logRecord2,
                logRecord3,
                logRecord4
        );
        LogFile logFile = new LogFile(logRecords);
        LogFileProcessor logFileProcessor = new LogFileProcessor(logFile);


        List<LogRecord> minimumCarPeriodLogRecords = logFileProcessor.getMinimumCarPeriod(recordCount, intervalSize);
        assertEquals(
                new LogRecord(sdf.parse("2020-01-01T13:00:00"), 15, 60),
                minimumCarPeriodLogRecords.get(0)
        );
        assertEquals(
                new LogRecord(sdf.parse("2020-01-01T12:30:00"), 17, 60),
                minimumCarPeriodLogRecords.get(1)
        );
    }


    @Test
    public void testGetMinimumCarPeriodWhenNoRecords() {
        int recordCount = 2;
        int intervalSize = 60;
        List<LogRecord> logRecords = Collections.emptyList();
        LogFile logFile = new LogFile(logRecords);
        LogFileProcessor logFileProcessor = new LogFileProcessor(logFile);


        List<LogRecord> minimumCarPeriodLogRecords = logFileProcessor.getMinimumCarPeriod(recordCount, intervalSize);
        assertEquals(0, minimumCarPeriodLogRecords.size());
    }


    @Test
    public void testGetTotalVehicleCountWhenNoRecords() {
        List<LogRecord> logRecords = Collections.emptyList();
        LogFile logFile = new LogFile(logRecords);
        LogFileProcessor logFileProcessor = new LogFileProcessor(logFile);


        int vehicleCount = logFileProcessor.getTotalVehicleCount();
        assertEquals(0, vehicleCount);
    }

    @Test
    public void testGetTotalVehicleCountNormalCase() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
        LogRecord logRecord1 = new LogRecord(sdf.parse("2020-01-01T12:30:00"), 10, 30);
        LogRecord logRecord2 = new LogRecord(sdf.parse("2020-01-01T13:00:00"), 7, 30);
        LogRecord logRecord3 = new LogRecord(sdf.parse("2020-01-01T13:30:00"), 8, 30);
        LogRecord logRecord4 = new LogRecord(sdf.parse("2020-01-01T14:00:00"), 9, 30);
        List<LogRecord> logRecords = Arrays.asList(
                logRecord1,
                logRecord2,
                logRecord3,
                logRecord4
        );
        LogFile logFile = new LogFile(logRecords);
        LogFileProcessor logFileProcessor = new LogFileProcessor(logFile);

        int vehicleCount = logFileProcessor.getTotalVehicleCount();
        assertEquals(34, vehicleCount);
    }


    @Test
    public void testMapToDateNormalCase() throws Exception {
        int noOfDates = 3;
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
        LogRecord logRecord1 = new LogRecord(sdf.parse("2020-01-01T12:30:00"), 10, 30);
        LogRecord logRecord2 = new LogRecord(sdf.parse("2020-01-01T13:00:00"), 7, 30);
        LogRecord logRecord3 = new LogRecord(sdf.parse("2020-01-02T13:30:00"), 8, 30);
        LogRecord logRecord4 = new LogRecord(sdf.parse("2020-01-03T14:00:00"), 9, 30);
        LogRecord logRecord5 = new LogRecord(sdf.parse("2020-01-03T15:00:00"), 9, 30);
        List<LogRecord> logRecords = Arrays.asList(
                logRecord1,
                logRecord2,
                logRecord3,
                logRecord4,
                logRecord5
        );
        LogFile logFile = new LogFile(logRecords);
        LogFileProcessor logFileProcessor = new LogFileProcessor(logFile);

        List<String> mappedToDay = logFileProcessor.mapToDay();
        assertEquals(noOfDates, mappedToDay.size());
        assertEquals("2020-01-01 17", mappedToDay.get(0));
        assertEquals("2020-01-02 8", mappedToDay.get(1));
        assertEquals("2020-01-03 18", mappedToDay.get(2));
    }

    @Test
    public void testMapToDateWhenDatesAreMissing() throws Exception {
        int noOfDates = 4;
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
        LogRecord logRecord1 = new LogRecord(sdf.parse("2020-01-01T12:30:00"), 10, 30);
        LogRecord logRecord2 = new LogRecord(sdf.parse("2020-01-01T13:00:00"), 7, 30);
        LogRecord logRecord3 = new LogRecord(sdf.parse("2020-01-02T13:30:00"), 8, 30);
        LogRecord logRecord4 = new LogRecord(sdf.parse("2020-01-06T14:00:00"), 9, 30);
        LogRecord logRecord5 = new LogRecord(sdf.parse("2020-01-08T15:00:00"), 9, 30);
        List<LogRecord> logRecords = Arrays.asList(
                logRecord1,
                logRecord2,
                logRecord3,
                logRecord4,
                logRecord5
        );
        LogFile logFile = new LogFile(logRecords);
        LogFileProcessor logFileProcessor = new LogFileProcessor(logFile);

        List<String> mappedToDay = logFileProcessor.mapToDay();
        assertEquals(noOfDates, mappedToDay.size());
        assertEquals("2020-01-01 17", mappedToDay.get(0));
        assertEquals("2020-01-02 8", mappedToDay.get(1));
        assertEquals("2020-01-06 9", mappedToDay.get(2));
        assertEquals("2020-01-08 9", mappedToDay.get(3));
    }

    @Test
    public void testMapToDateWhenNoRecordsAreGiven() {
        int noOfDates = 0;
        List<LogRecord> logRecords = Collections.emptyList();
        LogFile logFile = new LogFile(logRecords);
        LogFileProcessor logFileProcessor = new LogFileProcessor(logFile);

        List<String> mappedToDay = logFileProcessor.mapToDay();
        assertEquals(noOfDates, mappedToDay.size());
    }

    @Test
    public void testGetTopNRecords() throws Exception {
        int noOfRecords = 4;
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
        LogRecord logRecord1 = new LogRecord(sdf.parse("2020-01-01T12:30:00"), 10, 30);
        LogRecord logRecord2 = new LogRecord(sdf.parse("2020-01-01T13:00:00"), 7, 30);
        LogRecord logRecord3 = new LogRecord(sdf.parse("2020-01-02T13:30:00"), 18, 30);
        LogRecord logRecord4 = new LogRecord(sdf.parse("2020-01-06T14:00:00"), 9, 30);
        LogRecord logRecord5 = new LogRecord(sdf.parse("2020-01-08T15:00:00"), 19, 30);
        List<LogRecord> logRecords = Arrays.asList(
                logRecord1,
                logRecord2,
                logRecord3,
                logRecord4,
                logRecord5
        );
        LogFile logFile = new LogFile(logRecords);
        LogFileProcessor logFileProcessor = new LogFileProcessor(logFile);

        List<LogRecord> topNRecords = logFileProcessor.getTopNRecords(noOfRecords);
        assertEquals(noOfRecords, topNRecords.size());
        assertEquals(logRecord5, topNRecords.get(0));
        assertEquals(logRecord3, topNRecords.get(1));
        assertEquals(logRecord1, topNRecords.get(2));
        assertEquals(logRecord4, topNRecords.get(3));
    }


    @Test
    public void testGetTopNRecordsWhenNGreaterThanNoOfRecords() throws Exception {
        int noOfRecords = 10;
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
        LogRecord logRecord1 = new LogRecord(sdf.parse("2020-01-01T12:30:00"), 10, 30);
        LogRecord logRecord2 = new LogRecord(sdf.parse("2020-01-01T13:00:00"), 7, 30);
        LogRecord logRecord3 = new LogRecord(sdf.parse("2020-01-02T13:30:00"), 18, 30);
        LogRecord logRecord4 = new LogRecord(sdf.parse("2020-01-06T14:00:00"), 9, 30);
        LogRecord logRecord5 = new LogRecord(sdf.parse("2020-01-08T15:00:00"), 19, 30);
        List<LogRecord> logRecords = Arrays.asList(
                logRecord1,
                logRecord2,
                logRecord3,
                logRecord4,
                logRecord5
        );
        LogFile logFile = new LogFile(logRecords);
        LogFileProcessor logFileProcessor = new LogFileProcessor(logFile);

        List<LogRecord> topNRecords = logFileProcessor.getTopNRecords(noOfRecords);
        assertEquals(logRecords.size(), topNRecords.size());
        assertEquals(logRecord5, topNRecords.get(0));
        assertEquals(logRecord3, topNRecords.get(1));
        assertEquals(logRecord1, topNRecords.get(2));
        assertEquals(logRecord4, topNRecords.get(3));
        assertEquals(logRecord2, topNRecords.get(4));
    }

}
