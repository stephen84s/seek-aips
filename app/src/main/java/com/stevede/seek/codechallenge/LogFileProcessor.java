package com.stevede.seek.codechallenge;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Responsible for processing the LogFile to get the desired stats
 */
public class LogFileProcessor {
    private final LogFile logFile;

    public LogFileProcessor(LogFile logFile) {
        this.logFile = logFile;
    }

    public int getTotalVehicleCount() {
        return logFile.getLogRecords()
                .stream()
                .flatMapToInt(logRecord -> IntStream.of(logRecord.getVehicleCount()))
                .sum();
    }

    public List<String> mapToDay() {
        Map<String, Integer> dayVehicleCount = logFile.getLogRecords()
                .stream()
                .collect(
                        Collectors.groupingBy(logRecord -> {
                                    SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT_DAY);
                                    return sdf.format(logRecord.getDate());
                                },
                                Collectors.summingInt(LogRecord::getVehicleCount))
                );
        return dayVehicleCount
                .entrySet()
                .stream()
                .map(entry -> entry.getKey() + " " + entry.getValue())
                .collect(Collectors.toList());
    }

    /**
     * Gets the top N records where we encountered the most vehicles
     *
     * @param n - Number of records to return
     * @return List of logRecords
     */
    public List<LogRecord> getTopNRecords(int n) {
        int noOfRecords = this.logFile.getLogRecords().size();
        return this.logFile.getLogRecords()
                .stream()
                .sorted(((o1, o2) -> o2.getVehicleCount() - o1.getVehicleCount()))
                .collect(Collectors.toList())
                .subList(0, Math.min(n, noOfRecords));
    }

    /**
     * Return time periods of size X with least vehicles
     *
     * @param count            number of intervals to return
     * @param intervalSizeMins size of interval in minutes
     * @return List of LogRecords with intervalSize same as intervalSizeMins and the vehicle count
     * for that time and interval size.
     */
    public List<LogRecord> getMinimumCarPeriod(int count, int intervalSizeMins) {
        long intervalSizeMilliseconds = (long) intervalSizeMins * 60 * 1000;

        if (logFile.getLogRecords().isEmpty()) {
            return Collections.emptyList();
        }

        // Assumption LogRecords are sorted as per datetime
        List<LogRecord> logRecords = logFile.getLogRecords();
        long lastRecordEndTime =
                logRecords.get(logRecords.size() - 1).getDate().getTime()
                        + (long) logRecords.get(logRecords.size() - 1).getDurationWindowMinutes() * 60 * 1000;
        List<LogRecord> validRecords = logRecords.stream()
                .filter(r -> (lastRecordEndTime - r.getDate().getTime()) >= intervalSizeMilliseconds)
                .collect(Collectors.toList());
        List<LogRecord> logRecordsExpandedWindow = new ArrayList<>();

        // Cartesian product between the validRecords and all LogRecords to check
        // which records vehicle count can be used in which one.
        for (LogRecord logRecordOuter : validRecords) {
            int vehicleCount = 0;

            for (LogRecord logRecordInner : logRecords) {
                long timeDiff = logRecordInner.getDate().getTime() - logRecordOuter.getDate().getTime();
                if (timeDiff >= 0 && timeDiff < intervalSizeMilliseconds) {
                    vehicleCount += logRecordInner.getVehicleCount();
                }
            }
            logRecordsExpandedWindow.add(new LogRecord(logRecordOuter.getDate(), vehicleCount, intervalSizeMins));
        }

        // Finally, sort based on vehicle count and return only the requested number of records
        return logRecordsExpandedWindow
                .stream()
                .sorted(Comparator.comparingInt(LogRecord::getVehicleCount))
                .collect(Collectors.toList())
                .subList(0, count);
    }
}
