package com.stevede.seek.codechallenge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A LogFile is a collection of LogRecords
 */
public class LogFile {
    private final List<LogRecord> logRecords;

    public LogFile(List<LogRecord> logRecords) {
        this.logRecords = new ArrayList<>(logRecords);
    }

    public List<LogRecord> getLogRecords() {
        return Collections.unmodifiableList(logRecords);
    }

    @Override
    public String toString() {
        return logRecords
                .stream()
                .map(LogRecord::toString)
                .collect(Collectors.joining("\n"));
    }
}
