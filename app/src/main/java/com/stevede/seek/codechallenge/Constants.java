package com.stevede.seek.codechallenge;

public interface Constants {
    String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    String DATE_FORMAT_DAY = "yyyy-MM-dd";
}
