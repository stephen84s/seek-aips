package com.stevede.seek.codechallenge;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.SortedMap;

/**
 * Responsible for reading the log file on the disk and generating a corresponding LogFile object.
 * Kind of like a factory.
 */
public class LogFileReader {

    /**
     * Reads the file from the disk and gives us a LogFile object
     * @param logFilePath  logFile path on disk
     * @return LogFile object constructed from given logfile on disk, returns null if
     *      filePath is invalid. If a date is invalid the record is skipped.
     *
     */
    public LogFile readLogFile(String logFilePath) {
        int durationWindowInMinutes = 30;
        try(Scanner scanner = new Scanner(new FileReader(logFilePath))) {
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
            List<LogRecord> logRecords = new ArrayList<>();
            while(scanner.hasNextLine()) {
                String[] textTokens = scanner.nextLine().split(" ");

                try {
                    LogRecord logRecord = new LogRecord(
                            sdf.parse(textTokens[0]),
                            Integer.parseInt(textTokens[1]),
                            durationWindowInMinutes
                    );
                    logRecords.add(logRecord);
                } catch (ParseException e) {
                    System.out.println("Assumed as should not happen, record will be skipped");
                }
            }
            return new LogFile(logRecords);

        } catch (FileNotFoundException e) {
            System.out.println("File path is invalid");
            return null;
        }
    }
}
