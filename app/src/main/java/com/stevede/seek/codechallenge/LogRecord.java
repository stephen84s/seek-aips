package com.stevede.seek.codechallenge;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class LogRecord {
    private final Date date;

    private final int vehicleCount;

    private int durationWindowMinutes = 30;

    private String dateFormat = Constants.DATE_FORMAT;

    public LogRecord(Date date, int vehicleCount, int durationWindowMinutes) {
        this.date = date;
        this.vehicleCount = vehicleCount;
        this.durationWindowMinutes = durationWindowMinutes;
        this.dateFormat = Constants.DATE_FORMAT;
    }

    public LogRecord(Date date, int vehicleCount, int durationWindowMinutes, String dateFormat) {
        this.date = date;
        this.vehicleCount = vehicleCount;
        this.durationWindowMinutes = durationWindowMinutes;
        this.dateFormat = dateFormat;
    }


    public Date getDate() {
        return this.date;
    }

    public int getVehicleCount() {
        return vehicleCount;
    }

    public int getDurationWindowMinutes() {
        return durationWindowMinutes;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
        return sdf.format(this.date) + " " + this.vehicleCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LogRecord logRecord = (LogRecord) o;
        return vehicleCount == logRecord.vehicleCount && durationWindowMinutes == logRecord.durationWindowMinutes && date.equals(logRecord.date) && dateFormat.equals(logRecord.dateFormat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, vehicleCount, durationWindowMinutes, dateFormat);
    }
}
