# Signal Vehicle Counter Processor

## Assumptions

1. If a timestamp line is missing in the middle of the file, it is assumed to be 0 vehicles in the half hour following
   it.

## Running the Code

Running the code with the `sample.log` file included in the `sample` directory:- Note the contents of the file are
exactly the same as provided in the PDF.

Note we use gradle for building and running the Application from the CLI.

Building the application and running unit tests

```shell
gradle build
```

Running the application with the bundled sample.log file, note the file path needs to be absolute.

```shell
gradle run --args=$(pwd)/sample/sample.log
```


